﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ipvisionCrmApi.Data;

namespace CPR.DeletionJob
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new IpvisionCrmApiDbEntities())
            {
                var midnight = DateTime.Now.Date;
                var entries = db.XmoTempCprLogs.Where(x => x.CreatedDate < midnight).ToList();
                foreach (var entry in entries)
                {
                    db.XmoTempCprLogs.Remove(entry);
                    db.SaveChanges();
                }
            }
        }
    }
}
