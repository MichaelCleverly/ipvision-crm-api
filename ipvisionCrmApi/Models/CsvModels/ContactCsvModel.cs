﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CsvHelper.Configuration.Attributes;

namespace ipvisionCrmApi.Models.CsvModels
{
    public class ContactCsvModel
    {
        [Name("name")]
        public string Name { get; set; }
        [Name("email")]
        public string Email { get; set; }
        [Name("phone")]
        public string Phone { get; set; }
        [Name("phone2")]
        public string Phone2 { get; set; }
        [Name("phone3")]
        public string Phone3 { get; set; }
        [Name("phone4")]
        public string Phone4 { get; set; }
        [Name("internalcrmid1")]
        public string Internalcrmid1 { get; set; }
        [Name("internalcrmid2")]
        public string Internalcrmid2 { get; set; }
        [Name("internalcrmid3")]
        public string Internalcrmid3 { get; set; }
        [Name("internalcrmid4")]
        public string Internalcrmid4 { get; set; }
        [Name("internalcrmid5")]
        public string Internalcrmid5 { get; set; }
        [Name("internalcrmid6")]
        public string Internalcrmid6 { get; set; }

    }
}