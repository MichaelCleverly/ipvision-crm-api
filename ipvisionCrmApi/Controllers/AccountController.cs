﻿using System;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ipvisionCrmApi.Data;
using ipvisionCrmApi.Helpers;
using ipvisionCrmApi.Integrations.ipvision;
using ipvisionCrmApi.Integrations.Mitel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ipvisionCrmApi.Models;
using ipvisionCrmApi.ViewModels.Account;

namespace ipvisionCrmApi.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {


        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            var vm = new LoginVm()
            {
                ReturnUrl = returnUrl
            };
            return View(vm);
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginVm model)
        {
            Log.Info("Trying to log in");
            var ipvisionConsumer = new IpvisionApiConsumer();
            try
            {
                ipvisionConsumer.Login(model.Email, model.Password);
                MailAddress address = new MailAddress(model.Email);
                string host = address.Host;

                using (var db = new IpvisionCrmApiDbEntities())
                {
                    var customer = await db.Customers.FirstOrDefaultAsync(x => x.Domain == host);
                    if (customer == null)
                    {
                        customer = new Customer()
                        {
                            Domain = host,
                            Name = host,
                            Id = Guid.NewGuid()
                        };
                        db.Customers.Add(customer);
                        await db.SaveChangesAsync();
                    }
                    var ident = new ClaimsIdentity(
                        new[]
                        {
                            // adding following 2 claim just for supporting default antiforgery provider
                            new Claim(ClaimTypes.NameIdentifier, model.Email),
                            new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider",
                                "ASP.NET Identity", "http://www.w3.org/2001/XMLSchema#string"),
                            new Claim(ClaimTypes.Name, model.Email),
                        },
                        DefaultAuthenticationTypes.ApplicationCookie);

                    //Add user ID to claims
                    ident.AddClaim(new Claim("UserId", customer.Id.ToString()));

                    HttpContext.GetOwinContext().Authentication.SignIn(
                        new AuthenticationProperties { IsPersistent = model.RememberMe }, ident);

                }


            }
            catch (Exception e)
            {
                Log.Error(e.Message, e);
                TempData["ErrorMessage"] = "Ugyldigt brugernavn eller password";
                return View(model);
            }

            return RedirectToAction("Index", "Home");

        }


        public async Task<ActionResult> Logout()
        {
            HttpContext.GetOwinContext().Authentication.SignOut();
            return RedirectToAction("Login");
        }



    }
}