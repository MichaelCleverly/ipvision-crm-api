﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ipvisionCrmApi.Data;
using ipvisionCrmApi.Extensions;
using ipvisionCrmApi.Integrations.ipvision;
using ipvisionCrmApi.ViewModels.ClientSettings;
using ipvisionCrmApi.ViewModels.ClientSettings.SubModels;
using ipvisionCrmApi.ViewModels.IntegrationSettings;

namespace ipvisionCrmApi.Controllers
{
    [Authorize]
    public class ClientSettingsController : Controller
    {
        // GET: ClientSettings
        public async Task<ActionResult> Index()
        {
            var customerId = User.UserId();

            var vm = new EditVm();
            using (var db = new IpvisionCrmApiDbEntities())
            {
                var customer = await db.Customers.SingleAsync(x => x.Id == customerId);

                vm.DefaultIncomingOpenUrl = customer.DefaultIncomingOpenUrl;
                vm.DefaultOutgoingOpenUrl = customer.DefaultOutgoingOpenUrl;

            }
            return View("Edit", vm);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(EditVm model)
        {
            var customerId = User.UserId();

            using (var db = new IpvisionCrmApiDbEntities())
            {
                var customer = await db.Customers.SingleAsync(x => x.Id == customerId);

                if (customer.DefaultOutgoingOpenUrl != model.DefaultOutgoingOpenUrl ||
                    customer.DefaultIncomingOpenUrl != model.DefaultIncomingOpenUrl)
                {
                    customer.UrlsChangedDate = DateTime.Now;
                    customer.DefaultOutgoingOpenUrl = model.DefaultOutgoingOpenUrl;
                    customer.DefaultIncomingOpenUrl = model.DefaultIncomingOpenUrl;
                    await db.SaveChangesAsync();
                }

                TempData["SuccessMessage"] = "Ændringer gemt";
                return RedirectToAction("Index");


            }
        }

        public async Task<ActionResult> Users()
        {
            if (Session["IpvisibleAuthHeader"] == null)
            {
                TempData["ErrorMessage"] = "Session udløbet. Log ind igen";
                return RedirectToAction("Login", "Account");
            }
            var customerId = User.UserId();
            var ipvisionConsumer = new IpvisionApiConsumer();
            using (var db = new IpvisionCrmApiDbEntities())
            {
                var customer = await db.Customers.SingleAsync(x => x.Id == customerId);
                var disabledUsers = await db.DisabledMitelUsers.Where(x => x.CustomerId == customerId).ToListAsync();
                var allUsers = ipvisionConsumer.GetDomainUsers(customer.Domain);

                var usersWithStatus = allUsers.Select(x => new UserWithStatus()
                {
                    MitelUser = x,
                    IsDisabled = disabledUsers.FirstOrDefault(w => w.MitelUserId == x.UserId) != null
                }).ToList();

                var vm = new UsersVm()
                {
                    Users = usersWithStatus
                };

                return View(vm);

            }
        }

        [HttpPost]
        public async Task<ActionResult> EnableOrDisableUser(string mitelUserId)
        {
            using (var db = new IpvisionCrmApiDbEntities())
            {
                try
                {
                    var ipvisionConsumer = new IpvisionApiConsumer();
                    var customerId = User.UserId();
                    var customer = await db.Customers.SingleAsync(x => x.Id == customerId);

                    var disabledUser = await db.DisabledMitelUsers.SingleOrDefaultAsync(x =>
                        x.MitelUserId == mitelUserId && x.CustomerId == customerId);
                    if (disabledUser == null)
                    {
                        var allUsers = ipvisionConsumer.GetDomainUsers(customer.Domain);
                        var user = allUsers.First(x => x.UserId == mitelUserId);
                        db.DisabledMitelUsers.Add(new DisabledMitelUser()
                        {
                            CustomerId = customerId,
                            Id = Guid.NewGuid(),
                            MitelUserId = mitelUserId,
                            Email = user.Email
                        });
                    }
                    else
                    {
                        db.DisabledMitelUsers.Remove(disabledUser);
                    }

                    await db.SaveChangesAsync();
                }
                catch (Exception)
                {
                    TempData["ErrorMessage"] =
                        "Kunne ikke ændre status for bruger. Forsøg igen, eller kontakt ipvision";
                    return RedirectToAction("Users");
                }
            }

            TempData["SuccessMessage"] = "Brugerstatus ændret";
            return RedirectToAction("Users");
        }
    }
}