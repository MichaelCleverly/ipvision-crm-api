﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using ipvisionCrmApi.Data;
using ipvisionCrmApi.DTO;
using ipvisionCrmApi.Helpers;
using ipvisionCrmApi.Integrations.Zendesk;

namespace ipvisionCrmApi.Controllers
{
    public class ApiController : Controller
    {

        // GET: Api
        public async Task<ActionResult> GetCustomerByDomain(string domain)
        {
            using (var db = new IpvisionCrmApiDbEntities())
            {
                var customer = await db.Customers.FirstOrDefaultAsync(x => x.Domain == domain);
                if (customer == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                }
                var dto = new CustomerDTO()
                {
                    Name = customer.Name,
                    Domain = customer.Domain,
                    Id = customer.Id,
                    CrmUris = customer.CrmUris.Select(x => new CrmUriDTO()
                    {
                        Name = x.Name,
                        Uri = x.ZendeskConnectorId != null ? $"{x.ZendeskConnector.ZendeskUrl}/agent/users/[internalCrmId1]/requested_tickets" : x.Uri,
                        Id = x.Id
                    }).ToList()
                    
                };

                return Json(dto, JsonRequestBehavior.AllowGet);
            }

        }

        public async Task<ActionResult> GetContactsByCustomerIdAndCallerId(Guid customerId, string callerId)
        {
            try
            {
                using (var db = new IpvisionCrmApiDbEntities())
                {
                    var contacts =
                        await db.InternalCrmContacts.Where(x => x.CustomerId == customerId && (x.Phone1 == callerId || x.Phone2 == callerId || x.Phone3 == callerId || x.Phone4 == callerId)).ToListAsync();
                    var dtos = contacts.Select(x => new InternalCrmContactDTO()
                    {
                        InternalCrmId1 = x.InternalCrmId1,
                        InternalCrmId2 = x.InternalCrmId2,
                        InternalCrmId3 = x.InternalCrmId3,
                        InternalCrmId4 = x.InternalCrmId4,
                        InternalCrmId5 = x.InternalCrmId5,
                        InternalCrmId6 = x.InternalCrmId6,
                        Email = x.Email,
                        Phone1 = x.Phone1,
                        Phone2 = x.Phone2,
                        Phone3 = x.Phone3,
                        Phone4 = x.Phone4,
                        Id = x.Id,
                        CustomerId = x.CustomerId,
                        Name = x.Name
                    }).ToList();

                    //Check for zendesk connector
                    var zendeskConnector = await db.CrmUris
                        .Where(x => x.CustomerId == customerId && x.ZendeskConnectorId != null)
                        .Select(x => x.ZendeskConnector).FirstOrDefaultAsync();
                    if (zendeskConnector != null)
                    {
                        var zendeskUsers = ZendeskApiConsumer.SearchUsers(zendeskConnector.Email, zendeskConnector.ApiToken,
                            zendeskConnector.ZendeskUrl, callerId);
                        if (zendeskUsers != null)
                        {
                            foreach (var user in zendeskUsers)
                            {
                                dtos.Add(new InternalCrmContactDTO()
                                {
                                    Email = user.email,
                                    CustomerId = customerId,
                                    InternalCrmId1 = user.id.ToString(),
                                    Name = user.name,
                                    Phone1 = user.phone,

                                });
                            }
                        }
                        else
                        {
                            dtos.Add(new InternalCrmContactDTO()
                            {
                                Email = "Not Found",
                                CustomerId = customerId,
                                InternalCrmId1 = "Not Found",
                                Name = "Not Found",
                                Phone1 = "Not Found",

                            });
                        }
                    }

                    return Json(dtos, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Log.Error($"Error on GetContactsByCustomerIdAndCallerId: CustomerId: {customerId}. CallerId: {callerId}", e);
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        public async Task<ActionResult> GetClientDefaultSettings(Guid customerId)
        {
            using (var db = new IpvisionCrmApiDbEntities())
            {
                var customer = await db.Customers.SingleAsync(x => x.Id == customerId);
                var jsonObj = new
                {
                    DefaultOutgoingOpenUrl = customer.DefaultOutgoingOpenUrl,
                    DefaultIncomingOpenUrl = customer.DefaultIncomingOpenUrl,
                    UrlsChangedDate = customer.UrlsChangedDate?.ToString("O", CultureInfo.InvariantCulture)
                };

                return Json(jsonObj, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<ActionResult> IsUserDisabled(string domain, string email)
        {
            using (var db = new IpvisionCrmApiDbEntities())
            {
                var customer = await db.Customers.FirstOrDefaultAsync(x => x.Domain == domain);
                if (customer == null)
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }

                var userName = email.Substring(0, email.IndexOf("@"));
                var disabledUser =
                    await db.DisabledMitelUsers.FirstOrDefaultAsync(
                        x => x.CustomerId == customer.Id && x.MitelUserId == userName);
                if (disabledUser != null)
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }

                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<ActionResult> InsertCpr(string customerDomain, string cpr, string callerId)
        {
            using (var db = new IpvisionCrmApiDbEntities())
            {
                db.XmoTempCprLogs.Add(new XmoTempCprLog()
                {
                    CallerId = callerId,
                    CprNumber = cpr,
                    CreatedDate = DateTime.Now,
                    CustomerDomain = customerDomain
                });

                await db.SaveChangesAsync();
            }
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public async Task<ActionResult> GetCpr(string customerDomain, string callerId)
        {
            var authGuid = new Guid(Request.Headers.Get("x-auth-guid"));
            var correctAuthGuid = new Guid("1e8d6234-86c6-49d8-9720-6418ae91114b");
            if (authGuid != correctAuthGuid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }

            using (var db = new IpvisionCrmApiDbEntities())
            {
                var entry = await db.XmoTempCprLogs.FirstOrDefaultAsync(x =>
                    x.CallerId == callerId && x.CustomerDomain == customerDomain);

                var cpr = entry?.CprNumber ?? "";
                if (entry != null)
                {
                    db.XmoTempCprLogs.Remove(entry);
                    await db.SaveChangesAsync();
                }
                return Json(cpr, JsonRequestBehavior.AllowGet);
            }
        }
    }
}