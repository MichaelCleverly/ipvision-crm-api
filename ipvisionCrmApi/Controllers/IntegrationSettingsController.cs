﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ipvisionCrmApi.Data;
using ipvisionCrmApi.Extensions;
using ipvisionCrmApi.Integrations.Zendesk;
using ipvisionCrmApi.ViewModels.IntegrationSettings;
using Microsoft.Ajax.Utilities;

namespace ipvisionCrmApi.Controllers
{
    [Authorize]
    public class IntegrationSettingsController : Controller
    {
        // GET: Integrations
        public async Task<ActionResult> Index()
        {
            var customerId = User.UserId();

            var vm = new IndexVm();
            using (var db = new IpvisionCrmApiDbEntities())
            {
                var customer = await db.Customers.SingleAsync(x => x.Id == customerId);
                vm.CrmUris = await db.CrmUris.Where(x => x.CustomerId == customerId).ToListAsync();

            }
            return View(vm);
        }

        public async Task<ActionResult> Edit(int id)
        {
            var customerId = User.UserId();
            using (var db = new IpvisionCrmApiDbEntities())
            {
                var uri = await db.CrmUris.SingleAsync(x => x.Id == id && x.CustomerId == customerId);
                if (uri.ZendeskConnectorId != null)
                {
                    return RedirectToAction("CreateEditZendeskConnector", new {id = uri.Id});
                }

                return View("CreateEditIntegration", uri);
            }
        }

        public async Task<ActionResult> Create()
        {
            return View("CreateEditIntegration", new CrmUri());
        }

        public async Task<ActionResult> CreateEditZendeskConnector(int? id = null)
        {
            var customerId = User.UserId();
            var vm = new CrmUri();
            if (id != null)
            {
                using (var db = new IpvisionCrmApiDbEntities())
                {
                    vm = await db.CrmUris.Where(x =>
                        x.Id == id && x.ZendeskConnectorId != null && x.CustomerId == customerId)
                        .Include(x => x.ZendeskConnector.CrmUris)
                        .SingleAsync();
                }
            }
            return View(vm);
        }

        [HttpPost]
        public async Task<ActionResult> CreateEditZendeskConnector(CrmUri model)
        {
            var customerId = User.UserId();

            using (var db = new IpvisionCrmApiDbEntities())
            {
                var existing = await db.CrmUris.FirstOrDefaultAsync(x => x.CustomerId == customerId && x.Id == model.Id);
                if (existing != null)
                {
                    existing.Uri = "";
                    existing.Name = model.Name;
                    existing.ZendeskConnector.Email = model.ZendeskConnector.Email;
                    existing.ZendeskConnector.ApiToken = model.ZendeskConnector.ApiToken;
                    existing.ZendeskConnector.ZendeskUrl = model.ZendeskConnector.ZendeskUrl;
                }
                else
                {
                    var connectorDbo = db.ZendeskConnectors.Add(new ZendeskConnector()
                    {
                        ZendeskUrl = model.ZendeskConnector.ZendeskUrl,
                        Email = model.ZendeskConnector.Email,
                        ApiToken = model.ZendeskConnector.ApiToken,
                    });
                    await db.SaveChangesAsync();

                    db.CrmUris.Add(new CrmUri()
                    {
                        CustomerId = customerId,
                        CreatedDate = DateTime.Now,
                        Uri = "",
                        Name = model.Name,
                        ZendeskConnectorId = connectorDbo.Id
                    });
                }

                await db.SaveChangesAsync();
            }

            TempData["SuccessMessage"] = "Zendesk Connector oprettet";

            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<ActionResult> Delete(FormCollection form)
        {
            var id = Convert.ToInt32(form["id"]);
            var customerId = User.UserId();
            using (var db = new IpvisionCrmApiDbEntities())
            {
                var uri = await db.CrmUris.SingleAsync(x => x.Id == id && x.CustomerId == customerId);
                if (uri.ZendeskConnectorId != null)
                {
                    db.ZendeskConnectors.Remove(uri.ZendeskConnector);
                }
                db.CrmUris.Remove(uri);
                await db.SaveChangesAsync();

                TempData["SuccessMessage"] = "Integration slettet";

                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public async Task<ActionResult> Edit(CreateEditVm model)
        {
            var customerId = User.UserId();

            using (var db = new IpvisionCrmApiDbEntities())
            {
                var existing = await db.CrmUris.FirstOrDefaultAsync(x => x.CustomerId == customerId && x.Id == model.Id);
                if (existing != null)
                {
                    existing.Uri = model.Uri;
                    existing.Name = model.Name;
                }
                else
                {
                    db.CrmUris.Add(new CrmUri()
                    {
                        CustomerId = customerId,
                        CreatedDate = DateTime.Now,
                        Uri = model.Uri,
                        Name = model.Name
                    });
                }

                await db.SaveChangesAsync();
            }

            TempData["SuccessMessage"] = "Ændringer gemt";

            return RedirectToAction("Index");
        }



        [AllowAnonymous]
        public async Task<ActionResult> OpenUrl(Guid customerId, string callerId, int urlId)
        {
            using (var db = new IpvisionCrmApiDbEntities())
            {
                var finalContacts = new List<InternalCrmContact>();
                var vm = new SelectContactUrlVm();
                var uri = await db.CrmUris.FirstOrDefaultAsync(x => x.CustomerId == customerId && x.Id == urlId);
                if (uri == null)
                {
                    return View("NoUriSet");
                }
                //Step 1: Check if the URI is a zendesk Connector
                if (uri.ZendeskConnector != null)
                {

                    try
                    {
                        var zendeskUsers = ZendeskApiConsumer.SearchUsers(uri.ZendeskConnector.Email,
                        uri.ZendeskConnector.ApiToken, uri.ZendeskConnector.ZendeskUrl, callerId);
                        if (zendeskUsers != null)
                        {
                            foreach (var user in zendeskUsers)
                            {
                                finalContacts.Add(new InternalCrmContact()
                                {
                                    Email = user.email,
                                    CustomerId = customerId,
                                    Name = user.name,
                                    InternalCrmId1 = user.id.ToString(),
                                    Phone1 = user.phone,

                                });
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        TempData["ErrorMessage"] = e.Message;
                        return View("SelectContact", vm);
                    }

                    vm.Uri = $"{uri.ZendeskConnector.ZendeskUrl}/agent/users/[internalCrmId1]/requested_tickets";
                    vm.Contacts = finalContacts;
                    if (!finalContacts.Any())
                    {
                        return Redirect($"{uri.ZendeskConnector.ZendeskUrl}/agent/tickets/new/1");
                    }

                    if (finalContacts.Count == 1)
                    {
                        var finalUri = vm.Uri
                            .Replace("[internalCrmId1]", finalContacts.First().InternalCrmId1);
                        return Redirect(finalUri);
                    }
                    else
                    {
                        return View("SelectContact", vm);
                    }


                }


                //Otherwise, check in contacts
                var contacts =
                    await db.InternalCrmContacts.Where(x => x.CustomerId == customerId && x.Phone1 == callerId || x.Phone2 == callerId || x.Phone3 == callerId || x.Phone4 == callerId).ToListAsync();




                var crmIdPlaceHolders = uri.Uri.Split('[', ']').ToList();
                crmIdPlaceHolders = crmIdPlaceHolders.Where(x => x.Contains("internalCrmId")).ToList();

                if (!contacts.Any())
                {
                    return View("NoContactFound");
                }

 
                foreach (var contact in contacts)
                {
                    if (crmIdPlaceHolders.Contains($"internalCrmId1") && !string.IsNullOrEmpty(contact.InternalCrmId1))
                    {
                        finalContacts.Add(contact);
                        continue;
                    }

                    if (crmIdPlaceHolders.Contains($"internalCrmId2") && !string.IsNullOrEmpty(contact.InternalCrmId2))
                    {
                        finalContacts.Add(contact);
                        continue;
                    }

                    if (crmIdPlaceHolders.Contains($"internalCrmId3") && !string.IsNullOrEmpty(contact.InternalCrmId3))
                    {
                        finalContacts.Add(contact);
                        continue;
                    }

                    if (crmIdPlaceHolders.Contains($"internalCrmId4") && !string.IsNullOrEmpty(contact.InternalCrmId4))
                    {
                        finalContacts.Add(contact);
                        continue;
                    }

                    if (crmIdPlaceHolders.Contains($"internalCrmId5") && !string.IsNullOrEmpty(contact.InternalCrmId5))
                    {
                        finalContacts.Add(contact);
                        continue;
                    }

                    if (crmIdPlaceHolders.Contains($"internalCrmId6") && !string.IsNullOrEmpty(contact.InternalCrmId6))
                    {
                        finalContacts.Add(contact);
                        continue;
                    }
                }

                if (finalContacts.Count == 1)
                {
                    var finalUri = uri.Uri
                        .Replace("[internalCrmId1]", contacts.First().InternalCrmId1)
                        .Replace("[internalCrmId2]", contacts.First().InternalCrmId2)
                        .Replace("[internalCrmId3]", contacts.First().InternalCrmId3)
                        .Replace("[internalCrmId4]", contacts.First().InternalCrmId4)
                        .Replace("[internalCrmId5]", contacts.First().InternalCrmId5)
                        .Replace("[internalCrmId6]", contacts.First().InternalCrmId6);
                    return Redirect(finalUri);
                }

                vm.Uri = uri.Uri;
                vm.Contacts = finalContacts;

                return View("SelectContact", vm);

            }

        }
    }
}