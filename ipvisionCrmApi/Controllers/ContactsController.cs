﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CsvHelper;
using CsvHelper.Configuration;
using ipvisionCrmApi.Data;
using ipvisionCrmApi.Extensions;
using ipvisionCrmApi.Models.CsvModels;
using ipvisionCrmApi.ViewModels.Contacts;

namespace ipvisionCrmApi.Controllers
{
    [Authorize]
    public class ContactsController : Controller
    {
        // GET: Contacts
        public async Task<ActionResult> Index()
        {
            var customerId = User.UserId();
            var vm = new IndexVm();
            using (var db = new IpvisionCrmApiDbEntities())
            {
                vm.Contacts = await db.InternalCrmContacts.Where(x => x.CustomerId == customerId).ToListAsync();
            }
            return View(vm);
        }

        public ActionResult Import()
        {
            return View();
        }

        public async Task<PartialViewResult> GetUploadBatchResult(HttpPostedFileBase file)
        {
            // extract only the filename
            var fileName = Path.GetFileName(file.FileName).ToLower().Replace(".csv", "") + Guid.NewGuid() + ".csv";
            // store the file inside ~/App_Data/uploads folder
            var path = Path.Combine(Server.MapPath("~/App_Data/TEMP/"), fileName);
            file.SaveAs(path);

            var vms = await GetImportResult(path);

            System.IO.File.Delete(path);

            var viewModel = new ImportResultVm()
            {
                File = file,
                ResultItems = vms
            };

            return PartialView("_ImportResultPartial", viewModel);
        }


        [HttpPost]
        public async Task<ActionResult> Import(HttpPostedFileBase file, string deleteExisting)
        {
            // extract only the filename
            var fileName = Path.GetFileName(file.FileName);
            // store the file inside ~/App_Data/uploads folder
            var path = Path.Combine(Server.MapPath("~/App_Data/TEMP"), fileName);
            file.SaveAs(path);

            var vms = await GetImportResult(path);

            var customerId = User.UserId();

            using (var db = new IpvisionCrmApiDbEntities())
            {
                if (deleteExisting == "on")
                {
                    var existing = await db.InternalCrmContacts.Where(x => x.CustomerId == customerId).ToListAsync();
                    foreach (var c in existing)
                    {
                        db.InternalCrmContacts.Remove(c);
                    }
                }
                foreach (var contact in vms.Where(x => !x.IsInError))
                {
                    try
                    {
       
                        db.InternalCrmContacts.Add(new InternalCrmContact()
                        {
                            Phone1 = contact.ResultCrmContact.Phone1,
                            Phone2 = contact.ResultCrmContact.Phone2,
                            Phone3 = contact.ResultCrmContact.Phone3,
                            Phone4 = contact.ResultCrmContact.Phone4,
                            Email = contact.ResultCrmContact.Email,
                            Name = contact.ResultCrmContact.Name,
                            InternalCrmId1 = contact.ResultCrmContact.InternalCrmId1,
                            InternalCrmId2 = contact.ResultCrmContact.InternalCrmId2,
                            InternalCrmId3 = contact.ResultCrmContact.InternalCrmId3,
                            InternalCrmId4 = contact.ResultCrmContact.InternalCrmId4,
                            InternalCrmId5 = contact.ResultCrmContact.InternalCrmId5,
                            InternalCrmId6 = contact.ResultCrmContact.InternalCrmId6,
                            CustomerId = customerId,

                        });

                        await db.SaveChangesAsync();

                    }
                    catch (Exception e)
                    {
                        var inError = contact.IsInError;
                        //TODO: Lav en side hvor det endelige resultat vises, inkl. hvilke linjer der fejlede
                    }

                }
            }



            System.IO.File.Delete(path);


            TempData["SuccessMessage"] = "Kontakter importeret.";
            return RedirectToAction("Index");
        }

        public ActionResult Create()
        {
            return View(new InternalCrmContact());
        }

        [HttpPost]
        public async Task<ActionResult> Create(InternalCrmContact model)
        {
            using (var db = new IpvisionCrmApiDbEntities())
            {
                var customerId = User.UserId();
                model.CustomerId = customerId;
                db.InternalCrmContacts.Add(model);
                await db.SaveChangesAsync();
                TempData["SuccessMessage"] = "Kontakt oprettet";
                return RedirectToAction("Index");
            }
        }

        public async Task<ActionResult> Edit(long id)
        {
            using (var db = new IpvisionCrmApiDbEntities())
            {
                var customerId = User.UserId();
                var contact =
                    await db.InternalCrmContacts.FirstOrDefaultAsync(x => x.Id == id && x.CustomerId == customerId);
                if (contact == null)
                {
                    TempData["ErrorMessage"] = "Kontakt ikke fundet";
                    return RedirectToAction("Index");
                }

                return View(contact);
            }

        }

        [HttpPost]
        public async Task<ActionResult> Edit(InternalCrmContact model)
        {
            using (var db = new IpvisionCrmApiDbEntities())
            {
                var customerId = User.UserId();
                var contact =
                    await db.InternalCrmContacts.FirstOrDefaultAsync(x => x.Id == model.Id && x.CustomerId == customerId);
                if (contact == null)
                {
                    TempData["ErrorMessage"] = "Kontakt ikke fundet";
                    return RedirectToAction("Index");
                }

                contact.Name = model.Name;
                contact.Email = model.Email;
                contact.Phone1 = model.Phone1;
                contact.Phone2 = model.Phone2;
                contact.Phone3 = model.Phone3;
                contact.Phone4 = model.Phone4;

                contact.InternalCrmId1 = model.InternalCrmId1;
                contact.InternalCrmId2 = model.InternalCrmId2;
                contact.InternalCrmId3 = model.InternalCrmId3;
                contact.InternalCrmId4 = model.InternalCrmId4;
                contact.InternalCrmId5 = model.InternalCrmId5;
                contact.InternalCrmId6 = model.InternalCrmId6;

                await db.SaveChangesAsync();
                TempData["SuccessMessage"] = "Ændringer gemt";

                return RedirectToAction("Index");
            }
        }

        public async Task<List<ImportResultItemVm>> GetImportResult(string path)
        {
            var vms = new List<ImportResultItemVm>();
            var westernEncoding = Encoding.GetEncoding("windows-1254");
            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                NewLine = Environment.NewLine,
                Delimiter = ";",
                PrepareHeaderForMatch = args => args.Header.ToLower(),
                MissingFieldFound = null,
                HeaderValidated = null,
                Encoding = westernEncoding

            };
            using (var reader = new StreamReader(new FileStream(path, FileMode.Open), westernEncoding))
            using (var csv = new CsvReader(reader, config))
            {
                var records = csv.GetRecords<ContactCsvModel>();
                using (var db = new IpvisionCrmApiDbEntities())
                {
                    var index = 1;
                    foreach (var row in records)
                    {
                        try
                        {

                            if (!string.IsNullOrEmpty(row.Email) && !row.Email.IsValidEmail())
                            {
                                vms.Add(new ImportResultItemVm()
                                {
                                    IsInError = true,
                                    Line = index,
                                    ErrorMessage = "Forkert formateret mailadresse"
                                });
                                index++;
                                continue;
                            }

                            var vm = new ImportResultItemVm()
                            {
                                IsInError = false,
                                Line = index,
                            };

                            vm.ResultCrmContact = new InternalCrmContact()
                            {
                                Name = row.Name,
                                Phone1 = row.Phone,
                                Phone2 = row.Phone2,
                                Phone3 = row.Phone3,
                                Phone4 = row.Phone4,
                                Email = row.Email,
                                InternalCrmId1 = row.Internalcrmid1,
                                InternalCrmId2 = row.Internalcrmid2,
                                InternalCrmId3 = row.Internalcrmid3,
                                InternalCrmId4 = row.Internalcrmid4,
                                InternalCrmId5 = row.Internalcrmid5,
                                InternalCrmId6 = row.Internalcrmid6
                            };

                            vms.Add(vm);

                        }
                        catch (Exception e)
                        {
                            vms.Add(new ImportResultItemVm()
                            {
                                IsInError = true,
                                Line = index,
                                ErrorMessage = "Ukendt fejlen i rækken"
                            });
                        }

                        index++;
                    }

                    return vms;
                }
            }

            var customerId = User.UserId();
            


        }
    }
}