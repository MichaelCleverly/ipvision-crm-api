﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ipvisionCrmApi.Data;
using ipvisionCrmApi.DTO.ContactsApi;
using ipvisionCrmApi.Integrations.ipvision;
using ipvisionCrmApi.Integrations.Mitel;

namespace ipvisionCrmApi.Controllers
{
    public class ContactsApiController : Controller
    {
        // GET: ContactsApi
        [HttpPost]
        public async Task<HttpStatusCodeResult> Create(CreateContactsDto model)
        {
            try
            {
                var customer = await Authorize();
                using (var db = new IpvisionCrmApiDbEntities())
                {
                    if (model.DeleteExisting)
                    {
                        var existing = await db.InternalCrmContacts.Where(x => x.CustomerId == customer.Id)
                            .ToListAsync();
                        foreach (var c in existing)
                        {
                            db.InternalCrmContacts.Remove(c);
                        }
                    }

                    foreach (var contact in model.Contacts)
                    {
                        try
                        {

                            db.InternalCrmContacts.Add(new InternalCrmContact()
                            {
                                Phone1 = contact.Phone1,
                                Phone2 = contact.Phone2,
                                Phone3 = contact.Phone3,
                                Phone4 = contact.Phone4,
                                Email = contact.Email,
                                Name = contact.Name,
                                InternalCrmId1 = contact.InternalCrmId1,
                                InternalCrmId2 = contact.InternalCrmId2,
                                InternalCrmId3 = contact.InternalCrmId3,
                                InternalCrmId4 = contact.InternalCrmId4,
                                InternalCrmId5 = contact.InternalCrmId5,
                                InternalCrmId6 = contact.InternalCrmId6,
                                CustomerId = customer.Id,

                            });

                            await db.SaveChangesAsync();

                        }
                        catch (Exception e)
                        {

                        }

                    }
                }
            }
            catch (UnauthorizedAccessException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            catch (Exception e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);

        }

        private async Task<Customer> Authorize()
        {

            var req = HttpContext.Request;
            var auth = req.Headers["Authorization"];
            if (!string.IsNullOrEmpty(auth))
            {
                var cred = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(auth.Substring(6)))
                    .Split(':');
                var userName = cred[0];
                var password = cred[1];

                var ipvisionConsumer = new IpvisionApiConsumer();
                ipvisionConsumer.Login(userName, password);
                using (var db = new IpvisionCrmApiDbEntities())
                {
                    MailAddress address = new MailAddress(userName);
                    string host = address.Host;

                    var customer = await db.Customers.FirstOrDefaultAsync(x => x.Domain == host);
                    return customer;
                }
            }

            return null;
        }
    }
}