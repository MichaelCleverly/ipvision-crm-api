"use strict";

// Class Definition
var KTLoginGeneral = function() {

    var login = $('#kt_login');

    var showErrorMsg = function(form, type, msg) {
        var alert = $('<div class="alert alert-' + type + ' alert-dismissible" role="alert">\
			<div class="alert-text">'+msg+'</div>\
			<div class="alert-close">\
                <i class="flaticon2-cross kt-icon-sm" data-dismiss="alert"></i>\
            </div>\
		</div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        //alert.animateClass('fadeIn animated');
        KTUtil.animateClass(alert[0], 'fadeIn animated');
        alert.find('span').html(msg);
    }


    var handleSignUpFormSubmit = function() {
        $('#kt_login_signup_submit').click(function(e) {

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    fullname: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    },
                    rpassword: {
                        required: true,
                        equalTo: "#password"
                    },
                    agree: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

        });
    }

    // Public Functions
    return {
        // public functions
        init: function() {
            handleSignUpFormSubmit();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function () {

    $('.custom-select').select2();
    KTLoginGeneral.init();
});


var input = document.querySelector("#register-phone");
var iti = window.intlTelInput(input, {
    initialCountry: "dk"
});

$("#phone-or-email").on('input', function () {
    var val = $(this).val();
    if (val.length > 6) {
        $("#kt_phone_email_submit").prop('disabled', false);
    } else {
        $("#kt_phone_email_submit").prop('disabled', true);
    }
})

$("#kt_login_back").on('click',
    function (e) {
        e.preventDefault();
        var form = $("#email-phone-form");
        var loginForm = $("#login-form");

        loginForm.slideUp(300);
        form.slideDown(300);

    });

function CheckPhoneOrEmail() {
    var input = $("#phone-or-email").val();
    var spinner = $("#login-spinner");
    var form = $("#email-phone-form");

    if (input.length < 6) { return };

    spinner.show();
    form.hide();

    function success(data) {

        $("#login-message").hide();
        var loginForm = $("#login-form");

        //Case: User found and registration done
        if (data.UserFound && data.RegistrationDone) {
            $('.required').prop('required', function () {
                return $(this).is(':visible');
            });
            if (data.Phone == null) {
                $("#login-phone").hide();
            } else {
                $("#login-phone").val(data.Phone);
                $("#login-phone").show();
            }

            if (data.Email == null) {
                $("#login-email").hide();
            } else {
                $("#login-email").val(data.Email);
                $("#login-email").show();
            }
            loginForm.show();
            $("#Password").focus();

        } else {
            var createForm = $("#create-form");
            var socialRegisterForms = $("#social-register-options");

            $("#register-email").val(data.Email);
            $("#register-phone").val(data.Phone);
            $("#register-first-name").val(data.FirstName);
            $("#register-last-name").val(data.LastName);
            $("#register-company-name").val(data.CompanyName);
            if (data.Phone != null) {
                $("#register-email").focus();
            }
            if (data.Email != null) {
                $("#register-phone").focus();
            }

            createForm.show();
            socialRegisterForms.show();


        }




    }

    function error() {

    }

    function complete() {
        spinner.hide();
    }

    var url = "/account/PhoneOrEmailResult?input=" + input;
    doAjaxPost(url, null, success, error, complete);
}

$("#phone-or-email").on('change',
    function () {
        CheckPhoneOrEmail();
    });

$("#phone-or-email").on('keypress', function (e) {
    if (e.which == 13) {
        CheckPhoneOrEmail();
    }
});

$("#kt_phone_email_submit").on('click',
    function () {
        CheckPhoneOrEmail();
    });


$(".auth-input").on('change',
    function () {

        var createForm = $("#create-form");
        var socialRegisterForms = $("#social-register-options");
        var loginForm = $("#login-form");

        var input = $(this).val();
        var url = "/account/PhoneOrEmailResult?input=" + input;

        function success(data) {
     
            if (data.UserFound) {
                createForm.slideUp(300);
                socialRegisterForms.hide();
                loginForm.slideDown(300);

                if (data.Phone == null) {
                    $("#login-phone").hide();
                } else {
                    $("#login-phone").val(data.Phone);
                    $("#login-phone").show();
                    $('#login-message').slideDown(300);
                    $('#login-message').text('Phone number already in use. Please log in.');
                }

                if (data.Email == null) {
                    $("#login-email").hide();
                } else {
                    $("#login-email").val(data.Email);
                    $("#login-email").show();
                    $('#login-message').slideDown(300);
                    $('#login-message').text('Email address already in use. Please log in.');
                }
            }
        }

        doAjaxPost(url, null, success, null, null);

    });

$(".btn-signup").on('click', function (e) {
    e.preventDefault();
    $("#phone-country-code").val(iti.getSelectedCountryData().dialCode);

    var btn = $(this);
    $("#ValidationMethod").val(btn.data("valmethod"));

    var form = $("#create-form");
   

    form.validate({
        rules: {
            firstname: {
                required: true
            },
            lastname: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true
            },
            password: {
                required: true
            },
            rpassword: {
                required: true,
                equalTo: "#cpassword"
            },
            agree: {
                required: true
            }
        }
    });

    if (!form.valid()) {
        return false;
    }
    btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
    form.submit();
});



$("#login-form").submit(function () {

    $("#kt_login_signin_submit").addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
});