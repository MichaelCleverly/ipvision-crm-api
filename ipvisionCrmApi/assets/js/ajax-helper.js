﻿function doAjaxPost(url, data, successcallback, errorcallback, completecallback) {
    $.ajaxSetup({ cache: false });
    var jsonData = JSON.stringify(data);
    var parameters = {
        url: url,
        type: "POST",
        data: jsonData,
        contentType: 'application/json',
        success: function (data) {
            if (successcallback != undefined && successcallback != null) {
                successcallback(data);
            }
        },
        error: function (data) {
            if (errorcallback != undefined && errorcallback != null) {
                errorcallback(data);
            }
        },
        complete: function (data) {
            if (completecallback != undefined && completecallback != null) {
                completecallback(data);
            }
        },
    };

    $.ajax(parameters);
}

function doAjaxGet(url, successcallback, errorcallback, completecallback) {
    $.ajaxSetup({ cache: false });
    var parameters = {
        url: url,
        type: "GET",
        contentType: 'application/json',
        success: function (data) {
            if (successcallback != undefined && successcallback != null) {
                successcallback(data);
            }
        },
        error: function (data) {
            if (errorcallback != undefined && errorcallback != null) {
                errorcallback(data);
            } else {
                if (data.statusCode == 401) {
                    window.location.replace("/login");
                }
            }
        },
        complete: function (data) {
            if (completecallback != undefined && completecallback != null) {
                completecallback(data);
            }
        }
    };

    $.ajax(parameters);
}