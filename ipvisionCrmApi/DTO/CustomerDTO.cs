﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ipvisionCrmApi.DTO
{
    [DataContract]
    public class CustomerDTO
    {
        [DataMember]
        public System.Guid Id { get; set; }
        [DataMember]
        public string Domain { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public List<CrmUriDTO> CrmUris { get; set; }
    }
}