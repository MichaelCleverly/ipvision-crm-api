﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ipvisionCrmApi.DTO.ContactsApi
{
    [DataContract]
    public class CreateContactsDto
    {
        [DataMember]
        public bool DeleteExisting { get; set; }

        [DataMember]
        public List<ContactDto> Contacts { get; set; }
    }
}