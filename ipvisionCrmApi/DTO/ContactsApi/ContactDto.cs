﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ipvisionCrmApi.DTO.ContactsApi
{
    [DataContract]
    public class ContactDto
    {
        [DataMember]
        public string Phone1 { get; set; }
        [DataMember]
        public string Phone2 { get; set; }
        [DataMember]
        public string Phone3 { get; set; }
        [DataMember]
        public string Phone4 { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string InternalCrmId1 { get; set; }
        [DataMember]
        public string InternalCrmId2 { get; set; }
        [DataMember]
        public string InternalCrmId3 { get; set; }
        [DataMember]
        public string InternalCrmId4 { get; set; }
        [DataMember]
        public string InternalCrmId5 { get; set; }
        [DataMember]
        public string InternalCrmId6 { get; set; }
    }
}