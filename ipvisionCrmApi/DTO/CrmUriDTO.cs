﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ipvisionCrmApi.DTO
{
    [DataContract]
    public class CrmUriDTO
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Uri { get; set; }
        [DataMember]
        public string Name { get; set; }
    }
}