﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ipvisionCrmApi.Data;

namespace ipvisionCrmApi.ViewModels.IntegrationSettings
{
    public class SelectContactUrlVm
    {
        public List<InternalCrmContact> Contacts { get; set; }
        public string Uri { get; set; }
    }
}