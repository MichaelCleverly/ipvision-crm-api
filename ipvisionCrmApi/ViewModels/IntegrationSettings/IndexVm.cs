﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ipvisionCrmApi.Data;

namespace ipvisionCrmApi.ViewModels.IntegrationSettings
{
    public class IndexVm
    {
        public List<CrmUri> CrmUris { get; set; }

    }
}