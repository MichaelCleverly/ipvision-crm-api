﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ipvisionCrmApi.ViewModels.IntegrationSettings
{
    public class CreateEditVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Uri { get; set; }
    }
}