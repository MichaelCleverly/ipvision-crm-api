﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ipvisionCrmApi.ViewModels.ClientSettings
{
    public class EditVm
    {
        public string DefaultIncomingOpenUrl { get; set; }
        public string DefaultOutgoingOpenUrl { get; set; }
    }
}