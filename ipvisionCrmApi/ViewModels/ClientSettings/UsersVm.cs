﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ipvisionCrmApi.ViewModels.ClientSettings.SubModels;

namespace ipvisionCrmApi.ViewModels.ClientSettings
{
    public class UsersVm
    {
        public List<UserWithStatus> Users { get; set; }
    }
}