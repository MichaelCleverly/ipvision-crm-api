﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ipvisionCrmApi.Integrations.ipvision.Models;

namespace ipvisionCrmApi.ViewModels.ClientSettings.SubModels
{
    public class UserWithStatus
    {
        public MitelUserModel MitelUser { get; set; }
        public bool IsDisabled { get; set; }
    }
}