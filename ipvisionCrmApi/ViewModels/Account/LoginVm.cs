﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ipvisionCrmApi.ViewModels.Account
{
    public class LoginVm
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        public string ReturnUrl { get; set; }
    }
}