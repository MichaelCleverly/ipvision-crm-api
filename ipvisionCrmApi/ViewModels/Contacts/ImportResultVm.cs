﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ipvisionCrmApi.ViewModels.Contacts
{
    public class ImportResultVm
    {
        public HttpPostedFileBase File { get; set; }
        public List<ImportResultItemVm> ResultItems { get; set; }
    }
}