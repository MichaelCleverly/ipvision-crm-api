﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ipvisionCrmApi.Data;

namespace ipvisionCrmApi.ViewModels.Contacts
{
    public class ImportResultItemVm
    {
        public int Line { get; set; }
        public bool IsInError { get; set; }
        public string ErrorMessage { get; set; }
        public InternalCrmContact ExistingCrmContact { get; set; }
        public InternalCrmContact ResultCrmContact { get; set; }
    }
}