﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ipvisionCrmApi.Data;

namespace ipvisionCrmApi.ViewModels.Contacts
{
    public class IndexVm
    {
        public List<InternalCrmContact> Contacts { get; set; }
    }
}