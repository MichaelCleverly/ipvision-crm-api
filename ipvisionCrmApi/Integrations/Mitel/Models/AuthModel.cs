﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ipvisionCrmApi.Integrations.Mitel.Models
{
    public class AuthModel
    {
        public string ApiKey { get; set; }
        public DateTime ValidUntil { get; set; }
    }
}