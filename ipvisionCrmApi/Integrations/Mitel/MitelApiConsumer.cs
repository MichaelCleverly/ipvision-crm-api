﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using ipvisionCrmApi.Integrations.Mitel.Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;

namespace ipvisionCrmApi.Integrations.Mitel
{
    public class MitelApiConsumer
    {
        private readonly string _baseUrl;


        public MitelApiConsumer()
        {
            ServicePointManager.SecurityProtocol =
                SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            _baseUrl = ConfigurationManager.AppSettings["ScantalkBaseUrl"];
        }


        public AuthModel Login(string username, string password)
        {
            var client = new RestClient(_baseUrl);
            client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("login", DataFormat.Json);

            var response = client.Get(request);
            if (!response.IsSuccessful)
            {
                throw new UnauthorizedAccessException("Invalid username or password");
            }

            var apiKey = response.Headers.FirstOrDefault(x => x.Name == "x-api-key")?.Value?.ToString();
            if (string.IsNullOrEmpty(apiKey))
            {
                throw new UnauthorizedAccessException("No API key found for user");
            }

            return new AuthModel()
            {
                ApiKey = apiKey,
                ValidUntil = DateTime.Now.AddHours(2)
            };
        }
    }
}