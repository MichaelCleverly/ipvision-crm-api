﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ipvisionCrmApi.Integrations.Zendesk.Models
{
    [DataContract]
    public class SearchResult
    {
        [DataMember]
        public List<User> results { get; set; }
    }
}