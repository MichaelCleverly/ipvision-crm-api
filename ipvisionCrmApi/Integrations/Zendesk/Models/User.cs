﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ipvisionCrmApi.Integrations.Zendesk.Models
{
    [DataContract]
    public class User
    {
        [DataMember]
        public long id { get; set; }
        [DataMember]
        public string url { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public DateTime created_at { get; set; }
        [DataMember]
        public DateTime updated_at { get; set; }
        [DataMember]
        public string time_zone { get; set; }
        [DataMember]
        public string iana_time_zone { get; set; }
        [DataMember]
        public string phone { get; set; }
    }
}