﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ipvisionCrmApi.Integrations.Zendesk.Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;

namespace ipvisionCrmApi.Integrations.Zendesk
{
    public class ZendeskApiConsumer
    {
        public static List<User> SearchUsers(string username, string password, string baseUrl, string phone)
        {
            var client = new RestClient($"{baseUrl}/api/v2/");
            client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest($"search.json?query={phone} type:user", Method.GET);
            var response = client.Execute(request);

            if (response.IsSuccessful)
            {
                var result = JsonConvert.DeserializeObject<SearchResult>(response.Content);
                return result.results;
            }
            else
            {
                throw new Exception("Fejl i Zendesk API");
            }

            return null;
        }
    }
}