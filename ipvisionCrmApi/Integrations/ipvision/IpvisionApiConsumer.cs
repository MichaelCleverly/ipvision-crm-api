﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using ipvisionCrmApi.Helpers;
using ipvisionCrmApi.Integrations.ipvision.Models;
using ipvisionCrmApi.Integrations.Mitel.Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;

namespace ipvisionCrmApi.Integrations.ipvision
{
    public class IpvisionApiConsumer
    {
        private readonly string _baseUrl;


        public IpvisionApiConsumer()
        {
            ServicePointManager.SecurityProtocol =
                SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            _baseUrl = ConfigurationManager.AppSettings["IpvisionApiBaseUrl"];
        }

        public List<MitelUserModel> GetDomainUsers(string domain)
        {
            var authHeader = HttpContext.Current.Session["IpvisibleAuthHeader"];
            if (authHeader == null)
            {
                throw new Exception("Please log in again");
            }
            var client = new RestClient(_baseUrl);
            var userRequest = new RestRequest($"/mitel/clients/{domain}/users", DataFormat.Json);

            userRequest.AddHeader("Authorization", authHeader.ToString());
            var userResponse = client.Get(userRequest);
            var users = JsonConvert.DeserializeObject<List<MitelUserModel>>(userResponse.Content);
            return users;
        }

        public void Login(string username, string password)
        {
            try
            {
                var client = new RestClient(_baseUrl);


                var request = new RestRequest("api/v1/login", DataFormat.Json);
                var jsonBody = new
                {
                    user = new
                    {
                        login = username,
                        password = password
                    }
                };
                request.AddJsonBody(jsonBody);
                var response = client.Post(request);
                Log.Info($"Resonse from api: {response.StatusCode}. {response.Content}");
                if (!response.IsSuccessful)
                {
                    Log.Warn($"Error from ipvision API: {response.Content}");
                    throw new UnauthorizedAccessException("Ugyldigt brugernavn eller password");
                }

                var responseBody = JsonConvert.DeserializeObject<IpvisionResponseRoot>(response.Content);

                var authHeader = response.Headers.First(x => x.Name == "Authorization").Value;
                var userRequest = new RestRequest($"mitel/users/{responseBody.data.username}", DataFormat.Json);
                userRequest.AddHeader("Authorization", authHeader.ToString());
                HttpContext.Current.Session["IpvisibleAuthHeader"] = authHeader.ToString();
                var userResponse = client.Get(userRequest);
                Log.Info($"Response from login: Code: {userResponse?.StatusCode}. Content: {userResponse.Content}");
                var userResponseBody = JsonConvert.DeserializeObject<IpvisionMitelUserResponseModel>(userResponse.Content);
                if (!userResponseBody.administrativeRoles.Contains("ADMIN"))
                {
                    Log.Warn($"Error from ipvision API: Found following permissions: {string.Join(",", userResponseBody.administrativeRoles)}");
                    throw new UnauthorizedAccessException("Din bruger har ikke rettigheder til at rette i CRM integrationer.");
                }
            }
            catch (Exception e)
            {
                Log.Error("Error logging in", e);
                throw e;
            }
        }


    }
}