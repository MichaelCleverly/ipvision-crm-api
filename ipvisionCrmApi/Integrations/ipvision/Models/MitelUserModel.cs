﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ipvisionCrmApi.Integrations.ipvision.Models
{
    [DataContract]
    public class MitelUserModel
    {
        [DataMember(Name = "user_id")]
        public string UserId { get; set; }
        [DataMember(Name = "name")]
        public string Name { get; set; }
        [DataMember(Name = "email")]
        public string Email { get; set; }

    }
}