﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ipvisionCrmApi.Integrations.ipvision.Models
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 


    [DataContract]
    public class IpvisionMitelUserResponseModel
    {
        public string userId { get; set; }
        [DataMember(Name= "administrative_roles")]
        public List<string> administrativeRoles { get; set; }
    }


}