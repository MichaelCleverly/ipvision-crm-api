﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Newtonsoft.Json;

namespace ipvisionCrmApi.Integrations.ipvision.Models
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    [DataContract]
    public class ElemMatch
    {
        [JsonProperty("sim_card.user_id")]
        public int SimCardUserId { get; set; }
    }
    [DataContract]
    public class CellNumbers
    {
        [JsonProperty("$elemMatch")]
        public ElemMatch ElemMatch { get; set; }
    }
    [DataContract]
    public class CellNumber
    {
        [JsonProperty("$elemMatch")]
        public ElemMatch ElemMatch { get; set; }
    }
    [DataContract]
    public class Conditions
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public int? user_id { get; set; }
        [JsonProperty("sim_card.user_id")]
        public int? SimCardUserId { get; set; }
        [DataMember]
        public CellNumbers cell_numbers { get; set; }
        [DataMember]
        public CellNumber cell_number { get; set; }
    }
    [DataContract]
    public class Rule
    {
        [DataMember]
        public List<string> actions { get; set; }
        [DataMember]
        public List<string> subject { get; set; }
        [DataMember]
        public Conditions conditions { get; set; }
    }
    [DataContract]
    public class Client
    {
        public string id { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public object partner_id { get; set; }
        [DataMember]
        public string csbc_routing { get; set; }
        [DataMember]
        public string connect30_domain { get; set; }
        [DataMember]
        public string primary_contact { get; set; }
        [DataMember]
        public string admin_contact { get; set; }
        [DataMember]
        public string admin_email { get; set; }
        [DataMember]
        public string admin_phone { get; set; }
        [DataMember]
        public string uuid { get; set; }
        [DataMember]
        public DateTime created_at { get; set; }
        [DataMember]
        public DateTime updated_at { get; set; }
    }
    [DataContract]
    public class Role
    {
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string context { get; set; }
        [DataMember]
        public bool main { get; set; }
    }
    [DataContract]
    public class Data
    {
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public object uuid { get; set; }
        [DataMember]
        public int client_id { get; set; }
        [DataMember]
        public object partner_id { get; set; }
        [DataMember]
        public string connect30_user_id { get; set; }
        [DataMember]
        public DateTime created_at { get; set; }
        [DataMember]
        public DateTime updated_at { get; set; }
        [DataMember]
        public List<Rule> rules { get; set; }
        [DataMember]
        public int main_role { get; set; }
        [JsonProperty("is_super_admin?")]
        public bool IsSuperAdmin { get; set; }
        [DataMember]
        public string mitel_profile_link { get; set; }
        [DataMember]
        public string language { get; set; }
        [DataMember]
        public List<string> mitel_apis { get; set; }
        [DataMember]
        public Client client { get; set; }
        [DataMember]
        public List<Role> roles { get; set; }
    }
    [DataContract]
    public class IpvisionResponseRoot
    {
        [DataMember(Name="data")]
        public Data data { get; set; }
    }


}