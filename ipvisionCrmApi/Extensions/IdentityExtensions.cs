﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;

namespace ipvisionCrmApi.Extensions
{
    public static class IdentityExtensions
    {
        public static Guid UserId(this IPrincipal principal)
        {
            var user = principal as ClaimsPrincipal;
            var claim = user.Claims.First(x => x.Type == "UserId");

            return new Guid(claim.Value);
        }
    }
}