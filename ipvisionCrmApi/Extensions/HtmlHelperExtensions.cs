﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ipvisionCrmApi.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static string IsActive(this HtmlHelper html, string action = null, string controller = null)
        {
            string activeClass = "kt-menu__item--here";
            // detect current app state
            string actualAction = (string)html.ViewContext.RouteData.Values["action"];
            string actualController = (string)html.ViewContext.RouteData.Values["controller"];

            if (String.IsNullOrEmpty(controller))
                controller = actualController;

            if (String.IsNullOrEmpty(action))
                action = actualAction;

            return (controller == actualController && action == actualAction) ? activeClass : String.Empty;
        }
    }
}