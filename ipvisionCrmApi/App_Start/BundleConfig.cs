﻿using System.Web;
using System.Web.Optimization;

namespace ipvisionCrmApi
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/assets/vendors/general/jquery/dist/jquery.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/assets/vendors/general/jquery-validation/dist/jquery.validate.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css",
                      "~/assets/vendors/general/tether/dist/css/tether.css",
                "~/assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css",
                "~/assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css",
                "~/assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css",
                "~/assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css",
                "~/assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css",
                "~/assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css",
                "~/assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css",
                "~/assets/vendors/general/select2/dist/css/select2.css",
                "~/assets/vendors/general/ion-rangeslider/css/ion.rangeSlider.css",
                "~/assets/vendors/general/nouislider/distribute/nouislider.css",
                "~/assets/vendors/general/owl.carousel/dist/assets/owl.carousel.css",
                "~/assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css",
                "~/assets/vendors/general/dropzone/dist/dropzone.css",
                "~/assets/vendors/general/quill/dist/quill.snow.css",
                "~/assets/vendors/general/@yaireo/tagify/dist/tagify.css",
                "~/assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css",
                "~/assets/vendors/general/animate.css/animate.css",
                "~/assets/vendors/general/toastr/build/toastr.css",
                "~/assets/vendors/general/dual-listbox/dist/dual-listbox.css",
                 "~/assets/vendors/general/sweetalert2/dist/sweetalert2.css",
                "~/assets/vendors/general/socicon/css/socicon.css",
                "~/assets/vendors/custom/vendors/line-awesome/css/line-awesome.css",
                "~/assets/vendors/custom/vendors/flaticon/flaticon.css",
                "~/assets/vendors/custom/vendors/flaticon2/flaticon.css",
                "~/assets/vendors/general/@fortawesome/fontawesome-free/css/all.min.css",
                "~/assets/vendors/custom/datatables/datatables.bundle.css",
                      "~/assets/css/style.bundle.css",
                "~/assets/css/custom.css"));

            bundles.Add(new Bundle("~/bundles/scripts").Include(
                "~/assets/vendors/general/popper.js/dist/umd/popper.js",
                "~/assets/vendors/general/bootstrap/dist/js/bootstrap.min.js",
                "~/assets/vendors/general/moment/min/moment.min.js",
                "~/assets/vendors/general/jquery-form/dist/jquery.form.min.js",
                "~/assets/vendors/general/block-ui/jquery.blockUI.js",
                "~/assets/vendors/general/sticky-js/dist/sticky.min.js",
                "~/assets/vendors/general/markdown/lib/markdown.js",
                "~/assets/vendors/general/dropzone/dist/dropzone.js",
                "~/assets/vendors/general/bootstrap-markdown/js/bootstrap-markdown.js",
                "~/assets/vendors/custom/js/vendors/bootstrap-markdown.init.js",
                "~/assets/vendors/general/bootstrap-notify/bootstrap-notify.min.js",
                "~/assets/vendors/custom/js/vendors/bootstrap-notify.init.js",
                "~/assets/vendors/general/jquery-validation/dist/additional-methods.js",
                "~/assets/vendors/custom/js/vendors/jquery-validation.init.js",
                "~/assets/vendors/general/es6-promise-polyfill/promise.min.js",
                "~/assets/vendors/general/jquery.repeater/src/lib.js",
                "~/assets/vendors/general/jquery.repeater/src/jquery.input.js",
                "~/assets/js/scripts.bundle.js",
                "~/assets/vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js",
                "~/assets/vendors/custom/datatables/datatables.bundle.js",
                "~/assets/vendors/general/parsley/parsley.js",
                "~/assets/js/ajax-helper.js"
                //"~/assets/vendors/custom/js/vendors/bootstrap-timepicker.init.js",
                ));

            BundleTable.EnableOptimizations = false;
        }
    }
}
