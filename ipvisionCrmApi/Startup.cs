﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ipvisionCrmApi.Startup))]
namespace ipvisionCrmApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
